from celery import Celery
from collections import defaultdict

app = Celery("tasks", broker="sqs://guest:guest@elasticmq:9324/queue/celery")

app.conf.update(
    broker_transport_options={
        "visibility_timeout": 30,
        "predefined_queues": {
            "celery": {
                "backoff_policy": {i:5 for i in range(1000)},
                "access_key_id": "test",
                "secret_access_key": "test",
                "backoff_tasks": ["tasks.add"],
                "url": "http://elasticmq:9324/queue/celery",
            },
        },
    },
    task_acks_late=True,
    task_acks_on_failure_or_timeout=False,
)


@app.task
def add(x, y):
    raise ValueError("Error")
